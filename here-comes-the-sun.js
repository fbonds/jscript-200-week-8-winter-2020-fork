var count = 0;
var req;

function changeBackgroundColor () {
    document.body.style.backgroundColor = 'rgb(' + count + ',' + count + ',' + count + ')';

    if (++count > 255) {
        count = 0;
    }
    req = requestAnimationFrame(changeBackgroundColor);
};

changeBackgroundColor();