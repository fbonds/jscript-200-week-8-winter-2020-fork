describe('soccer fucntions', () => {
    describe('getTotalPoints', () => {
        // pass
        it('Should give me 7 points for "wwdl"', () => {
            const points = getTotalPoints('wwdl');

            expect(points).toBe(7);
        });
        // fail 
        it('Should give me 5 points for "ddddd"', () => {
            const points = getTotalPoints('ddddd');

            expect(points).toBe(5);
        });
    });

    describe('orderTeams', () => {
        const team = {
            name: "Sounders",
            results: "wdlw"
        };
        it('Should output correct "team name: points"', () => {
            const output = (orderTeams(team));
            expect(output).toBe('Sounders: 7');
        });
    });
});
