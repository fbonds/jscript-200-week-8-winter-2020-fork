const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    const random = Math.random();
    if (random > 0.5) {
      resolve(random);
      console.log('success');
    } else {
      reject(random);
      console.log('fail');
    } console.log('complete');
  }, 1000);
});