
describe('Blackjack', () => {
    describe('Tests', () => {
        //get the deck
        const deck = getDeck();
        //get the necessary cards for the tests
        //couldn't figure out how to grab cards other than randomly but
        //saw that the deck always created in the same order, so just grabbed
        //necessary cards by index
        const two = deck[4];
        const five = deck[7];
        const six = deck[8];
        const seven = deck[9];
        const nine = deck[11];
        const ten = deck[12];
        const ace = deck[3];
      

        it('calcPoints: No Aces case', () => {
            const myhand = [];
            myhand.push(deck[9]);
            myhand.push(deck[12]);
            const cardCount = calcPoints(myhand);
            const expected = {
                total: 17,
                isSoft: false
            };
            expect(cardCount).toEqual(expected);
        });

        it('calcPoints: 1 Ace case (soft)', () => {
            const myhand = [];
            myhand.push(seven);
            myhand.push(ace);
            const cardCount = calcPoints(myhand);
            const expected = {
                total: 18,
                isSoft: true
            };
            expect(cardCount).toEqual(expected);
        });
        it('calcPoints: 1 Ace case (not soft)', () => {
            const myhand = [];
            myhand.push(seven);
            myhand.push(ten);
            myhand.push(ace);
            const cardCount = calcPoints(myhand);
            const expected = {
                total: 18,
                isSoft: false
            };
            expect(cardCount).toEqual(expected);

        });
        it('calcPoints: multiple Ace case (soft)', () => {
            const myhand = [];
            myhand.push(ace);
            myhand.push(ace);
            const cardCount = calcPoints(myhand);
            const expected = {
                total: 12,
                isSoft: true
            };
            expect(cardCount).toEqual(expected);
        });
        it('calcPoints: multiple Ace case (not soft)', () => {
            const myhand = [];
            myhand.push(seven);
            myhand.push(ten);
            myhand.push(ace);
            myhand.push(ace);
            const cardCount = calcPoints(myhand);
            const expected = {
                total: 19,
                isSoft: false
            };
            expect(cardCount).toEqual(expected);

        });
        it('dealerShouldDraw: 10 + 9 returns FALSE case', () => {
            const myhand = [];
            myhand.push(ten);
            myhand.push(nine);
            const shouldDraw = dealerShouldDraw(myhand);
            const expected = false;
            expect(shouldDraw).toEqual(expected);
        });
        it('dealerShouldDraw: Ace + 6 returns TRUE case', () => {
            const myhand = [];
            myhand.push(ace);
            myhand.push(six);
            const shouldDraw = dealerShouldDraw(myhand);
            const expected = true;
            expect(shouldDraw).toEqual(expected);
        });
        it('dealerShouldDraw: 10 + 6 + Ace returns FALSE case', () => {
            const myhand = [];
            myhand.push(ten);
            myhand.push(six);
            myhand.push(ace);
            const shouldDraw = dealerShouldDraw(myhand);
            const expected = false;
            expect(shouldDraw).toEqual(expected);
        });
        it('dealerShouldDraw: 2 + 5 + 2 + 5 returns TRUE case', () => {
            const myhand = [];
            myhand.push(two);
            myhand.push(five);
            myhand.push(two);
            myhand.push(five);
            const shouldDraw = dealerShouldDraw(myhand);
            const expected = true;
            expect(shouldDraw).toEqual(expected);
        });
    });
});