const formEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');

const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists/';

formEl.addEventListener('submit', function(e) {
  e.preventDefault();

  const year = yearEl.value;
  const month = monthEl.value;
  const date = dateEl.value;
  const searchDate = year + '-' + month + '-' + date;
  const url = `${BASE_URL}/${searchDate}/hardcover-fiction.json?api-key=${API_KEY}`

  fetch(url)
    .then(function(data){
      return data.json();
    })
    .then(function(responseJson){
      [0,1,2,3,4].forEach(function(i) {
        let book = responseJson.results.books[i];

        const bookTitle = book.title;
        const bookAuthor = book.author;
        const bookDesc = book.description
        const thisBook = 'book' + `${i}`
        const imgURL = book.book_image;

        document.getElementById(thisBook).src = imgURL;
        document.getElementById(thisBook).innerHTML = 
        '<img src="' + imgURL + '"><br/>' + `Title: ${bookTitle}` + '<br/>' + `Author: ${bookAuthor}` + '<br/>' + `Description: ${bookDesc}` + '<br/>';
      });
    });

});
